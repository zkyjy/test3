﻿using QRST_DI_Resources;
using QRST_DI_SS_DBInterfaces.IDBEngine;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading.Tasks;

namespace FindMissGFdata2Table
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {

            try
            {
                if(!Constant.ServiceIsConnected)
                {
                    Constant.InitializeTcpConnection();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("获取TCP连接异常："+ex);
                Console.ReadLine();
            }
            List<string> StoreIPList = new List<string>() {"10.10.1.14",
            "10.0.1.15",
            "10.0.1.16",
            "10.0.1.17",
            "10.0.1.18",
            "10.0.1.19",
            "10.0.1.20",
            "10.0.1.21",
            "10.0.1.22",
            "10.0.1.23",
            "10.0.1.24",
            "10.0.1.25",
            "10.0.1.26",
            "10.0.1.27",
            "10.0.1.28",
            "10.0.1.29",
            "10.0.1.124",
            "10.0.1.126",
            "10.0.1.128"};
            string dbip = "10.0.1.110";
            //List<string> StoreIPList = new List<string>() { "192.168.10.109" };
            //string dbip = "192.168.10.109";

            string maxid = "-1";
            string constr = "server =" + dbip + "; user id = HJDATABASE_ADMIN; password = dbadmin_2011; charset = utf8; database = evdb";
            string sql = string.Format("SELECT  id, QRST_CODE, Name,ImportTime from prod_gf1 WHERE id >{0} ORDER BY ID  LIMIT 10000;", maxid);
            //QRST_DI_DS_Basis.DBEngine.MySqlBaseUtilities mysqlutil = new QRST_DI_DS_Basis.DBEngine.MySqlBaseUtilities(constr);
            IDbBaseUtilities baseUtilities = Constant.IdbServerUtilities.GetSubDbUtilByCon(constr);
            DataSet ds = baseUtilities.GetDataSet(sql);
            List<Task> tasks = new List<Task>();

            while (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                maxid = ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1]["id"].ToString();
                tasks.Add(Task.Factory.StartNew(objds =>
                {
                    DataSet myds = objds as DataSet;
                    foreach (DataRow dr in myds.Tables[0].Rows)
                    {
                        string id = dr["id"].ToString();
                        string name = dr["Name"].ToString();
                        string code = dr["QRST_CODE"].ToString();
                        string importtime = dr["ImportTime"].ToString();

                        string relFilePath = GetRelateDataPath(name);
                        bool exist = false;
                        if (relFilePath != "")
                        {
                            foreach (var ip in StoreIPList)//循环遍历历史磁盘
                            {
                                string datapath = string.Format(@"\\{0}\zhsjk\{1}", ip, relFilePath);
                                if (File.Exists(datapath))
                                {
                                    exist = true;
                                    break;
                                }
                            }
                        }

                        if (!exist)
                        {
                            Console.WriteLine("线程" + Task.CurrentId.ToString() + "发现MissData:" + code);
                            sql = string.Format("insert into DataMissRecord (id,QRST_CODE,Name,ImportTime)values({0},'{1}','{2}','{3}')", id, code, name, importtime);
                            try
                            {
                                baseUtilities.ExecuteSql(sql);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("线程" + Task.CurrentId.ToString() + "插入失败:" + ex.Message);
                            }
                        }
                    }
                }, ds.Copy()));

                Console.WriteLine("启动线程" + tasks[tasks.Count - 1].Id.ToString());


                sql = string.Format("SELECT  id, QRST_CODE, Name,ImportTime from prod_gf1 WHERE id >{0} ORDER BY ID  LIMIT 10000;", maxid, 10000);
                ds = baseUtilities.GetDataSet(sql);

            }

            Task.WaitAll(tasks.ToArray());
            Console.WriteLine("OK");
            Console.ReadKey();

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                Console.ReadLine();
            }
        }



        //GF1_WFV1_E114.1_N22.9_20130712_L1A0000052397.tar.gz
        public static string GetRelateDataPath(string Name)
        {
            string[] strArr = Name.Split("_".ToCharArray());
            if (strArr.Length == 6)
            {
                string satellite = strArr[0];
                string sensor = strArr[1];
                string year = strArr[4].Substring(0, 4);
                string month = strArr[4].Substring(4, 2);
                string day = strArr[4].Substring(6, 2);
                return string.Format("实验验证数据库\\GF1卫星数据\\{0}\\{1}\\{2}\\{3}\\{4}\\{5}\\{6}", satellite, sensor, year, month, day, Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(Name)), Name);
            }
            return "";
        }
    }
}
